# centos7-movescreen
[![build status](https://gitlab.com/harbottle/centos7-movescreen/badges/master/build.svg)](https://gitlab.com/harbottle/centos7-movescreen/builds)

`movescreen` RPM package for CentOS 7. Built for the [harbottle-main](https://gitlab.com/harbottle/harbottle-main) repo.

## Installation

```bash
# Install EPEL repo
yum -y install epel-release

# Install harbottle-main repo
yum -y install https://harbottle.gitlab.io/harbottle-main/7/x86_64/harbottle-main-release.rpm

# Install movescreen
yum -y install movescreen
```

## More info

This project builds a CentOS 7 RPM for [movescreen](https://github.com/liger1978/movescreen).
